Panels CreateNode
http://drupal.org/project/panels_createnode

SUMMARY:
This module adds a "Create node" section to the "Add content" window of panels. This lets you create and attach nodes on the fly without leaving the "panel content" page.

REQUIREMENTS:
1. http://drupal.org/project/panels

INSTALLATION:
Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-5-6 for further information.

UPGRADING:

CONFIGURATION:
1. Visit module configuration page at /admin/build/panels/settings/panels_createnode;
2. Select node types, that you want to see in Panels Create node;
3. Set node view default values;
4. If the user doesn't have "control node view" permissions, the "node view settings" fieldset won't be displayed when creating a node. In this case default values will be used.

SPONSORSHIP
This project is sponsored by FreshFX Media GmbH
www.freshfx.at

CONTACTS:
  Nikit - http://nikiit.ru/contact

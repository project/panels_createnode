<?php
/**
 * @file
 * CreateNode plugin.
 */

/**
 * Initial array for CreateNode plugin.
 */
$plugin = array(
  'title' => t('Create node'),
  'content type' => 'panels_createnode_createnode_content_type',
  'render callback' => 'panels_createnode_content_type_render',  
  'edit form' => 'panels_createnode_content_type_edit_form',
  'admin title' => 'panels_createnode_content_type_admin_title',
  'admin info' => 'panels_createnode_content_type_admin_info',
  'edit text' => t('Edit node'),
);

/**
 * Provide administrative title for CreateNode plugin.
 */
function panels_createnode_content_type_admin_title($subtype, $conf, $context = NULL) {
  $output = t('Create node');
  if ($conf['override_title'] && !empty($conf['override_title_text'])) {
    $output = filter_xss_admin($conf['override_title_text']);
  }
  else {
    if (isset($conf['node_nid'])) {
      $nid = $conf['node_nid'];
      $node = node_load($nid);
      if ($node) {
        $output = $node->type;
      }
      else {
        $output = t('Error');
      }
    }
  }
  return $output;
}

/**
 * Provide administrative info for CreateNode plugin.
 */
function panels_createnode_content_type_admin_info($subtype, $conf, $context = NULL) {
  $block = new stdClass();

  if (isset($conf['node_nid'])) {
    $nid = $conf['node_nid'];
    $node = node_load($nid);
    if ($node) {
      $block->title = $node->title;
      
      $teaser = $conf['node_form_teaser'] ? $conf['node_form_teaser'] : variable_get('panels_createnode_teaser', FALSE);
      $page = $conf['node_form_page'] ? $conf['node_form_page'] : variable_get('panels_createnode_page', FALSE);
      $links = $conf['node_form_links'] ? $conf['node_form_links'] : variable_get('panels_createnode_links', FALSE);
      $block->content = node_view($node, $teaser, $page, $links);
    }
    else {
      $block->title = t('Node #!nid could not be found.', array('!nid' => $nid));
      $block->content = '';
    }
  }
  
  return $block;
}

/**
 * Return the content type available for CreateNode plugin by the specified $subtype_id.
 */
function panels_createnode_createnode_content_type_content_type($subtype_id) {
  $nodetypes = node_get_types('types');
  if ($nodetypes[$subtype_id]) {
    return _panels_createnode_createnode_content_type($nodetypes, $subtype_id);
  }
}

/**
 * Return all content types available for CreateNode plugin.
 */
function panels_createnode_createnode_content_type_content_types() {
  $types = array();

  $nodetypes = node_get_types('types');
  
  foreach (variable_get('panels_createnode_nodetypes', array()) as $nodetype => $enabled) {
    if (!$enabled) continue;
    if (node_access('create', $nodetype)) {
      $info = _panels_createnode_createnode_content_type($nodetypes, $nodetype);
      if ($info) {
        $types[$nodetype] = $info;
      }
    }
  }
  
  return $types;
}

/**
 * Build CreateNode plugin by specified node type.
 */
function _panels_createnode_createnode_content_type($nodetypes, $nodetype) {
  return array(
    'title' => $nodetypes[$nodetype]->name,
    'icon' => 'createnode.png',
    'category' => t('Create node'),
  );
}

/**
 * CreateNode plugin output function.
 */
function panels_createnode_content_type_render($subtype, $conf, $args, $context) {
  if (!isset($conf['node_nid'])) {
    return ;
  }
  $nid = $conf['node_nid'];
  $node = node_load($nid);
  
  $block = new stdClass();
  
  if ($node) {
    $block->title = $node->title;
    $teaser = $conf['node_form_teaser'] ? $conf['node_form_teaser'] : variable_get('panels_createnode_teaser', FALSE);
    $page = $conf['node_form_page'] ? $conf['node_form_page'] : variable_get('panels_createnode_page', FALSE);
    $links = $conf['node_form_links'] ? $conf['node_form_links'] : variable_get('panels_createnode_links', FALSE);
    $block->content = node_view($node, $teaser, $page, $links);
    
    if (node_access('update', $node)) {
      $block->admin_links = array(
        array(
          'title' => t('Edit node'),
          'alt' => t("Edit this node"),
          'href' => "node/{$node->nid}/edit",
          'query' => drupal_get_destination(),
        ),
      );
    }
    
  }
  else {
    $block->title = t('Node #!nid could not be found.', array('!nid' => $nid));
    $block->content = '';
  }
  return $block;
}

/**
 * CreateNode plugin form.
 * Attaching node add/edit form.
 */
function panels_createnode_content_type_edit_form(&$form, &$form_state) {
  global $user;
  global $language;
  
  $nodetypes = node_get_types('types');
  $type = $form_state['subtype_name'];
  
  module_load_include('inc', 'node', 'node.pages');
  
  $conf = $form_state['conf'] ? $form_state['conf'] : array();
  
  if (isset($conf['node_nid'])) {
    $nid = $conf['node_nid'];
    $node = node_load($nid);
    
    if (!node_access('update', $node)) {
      return ;
    }
  }
  else {
    $node = array('uid' => $user->uid, 'name' => $user->name, 'type' => $type, 'language' =>  $language->language);
    
    if (!node_access('create', $node)) {
      return ;
    }
  }
  
  $form_state['node_form_state'] = empty($form_state['node_form_state']) ? array() : $form_state['node_form_state'];
  
  $form['node_title'] = array('#type' => 'item', '#value' => '<h3>' . t('Create @name', array('@name' => $nodetypes[$type]->name)) . '</h3>');
  $form['node_form'] = drupal_retrieve_form($type .'_node_form', $form_state['node_form_state'], $node);
  drupal_prepare_form($type .'_node_form', $form['node_form'], $form_state['node_form_state']);
  
  unset($form['node_form']['#validate']);
  unset($form['node_form']['#submit']);
  unset($form['node_form']['buttons']);

  if (user_access('control node view form')) {
    $form['node_form_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node view settings'),
    );
    $form['node_form_settings']['node_form_teaser'] = array(
      '#type' => 'checkbox',
      '#title' => t('Teaser'),
      '#default_value' => $conf['node_form_teaser'] ? $conf['node_form_teaser'] : variable_get('panels_createnode_teaser', FALSE),
      '#description' => t('Whether to display the teaser only or the full form.'),
    );
    $form['node_form_settings']['node_form_page'] = array(
      '#type' => 'checkbox',
      '#title' => t('Page'),
      '#default_value' => $conf['node_form_page'] ? $conf['node_form_page'] : variable_get('panels_createnode_page', TRUE),
      '#description' => t('Whether the node is being displayed by itself as a page.'),
    );
    $form['node_form_settings']['node_form_links'] = array(
      '#type' => 'checkbox',
      '#title' => t('Links'),
      '#default_value' => $conf['node_form_links'] ? $conf['node_form_links'] : variable_get('panels_createnode_links', FALSE),
      '#description' => t('Whether or not to display node links. Links are omitted for node previews.'),
    );
  }
  
  return $form;
}

/**
 * CreateNode plugin form submit function.
 */
function panels_createnode_content_type_edit_form_submit(&$form, &$form_state) {
  $node = (object) $form_state['values'];
  $form_state['values']['op'] = t('Submit');
  node_save($node);
  if ($node->nid) {
    $form_state['conf']['node_nid'] = $node->nid;
    if (user_access('control node view')) {
      $form_state['conf']['node_form_teaser'] = $form_state['values']['node_form_teaser'];
      $form_state['conf']['node_form_page'] = $form_state['values']['node_form_page'];
      $form_state['conf']['node_form_links'] = $form_state['values']['node_form_links'];
    }
  }
}
